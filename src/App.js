import React, { useEffect, useState } from "react";
import { selectAll } from "./firebase";
import GroupGenerator from "./Components/GroupGenerator";

function App() {
	const [users, setUsers] = useState([]);

	useEffect(() => {
		selectAll("users").then((res) => {
			const userList = res.filter(user => user.isActive);
			setUsers(userList);
		});
	}, []);

	return (
		<div className="h-full">
			<GroupGenerator users={users} />
		</div>
	);
}

export default App;
