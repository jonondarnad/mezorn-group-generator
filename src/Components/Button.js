import React from "react";

const Button = ({ children, onClick, minus }) => {
  const plus = `bg-gradient-to-r from-yellow-400 to-orange-300`;
  const minuss = `bg-gradient-to-r from-orange-600 to-red-700`;
  return (
    <div
      onClick={onClick}
      className={`flex justify-center px-6 py-2 border-2 border-white cursor-pointer text-white font-medium text-xs leading-tight uppercase rounded hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out hover:bg-white my-2`}
    >
      {children}
    </div>
  );
};
export { Button };
