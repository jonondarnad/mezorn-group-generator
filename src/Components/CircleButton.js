const CircleButton = ({ children, onClick, minus }) => {
  const plus = `bg-green-700 text-white leading-normal shadow-md hover:bg-green-800 hover:shadow-lg  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-900 active:shadow-lg`;
  const minuss = `bg-red-700 text-white leading-normal shadow-md hover:bg-red-800 hover:shadow-lg  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-900 active:shadow-lg`;
  return (
    <div className="flex space-x-2 justify-center" onClick={onClick}>
      <div>
        <button
          type="button"
          className={`rounded-full p-2 ${
            minus ? minuss : plus
          } transition duration-150 ease-in-out w-14 h-14`}
        >
          <div className="text-xs">{children}</div>
        </button>
      </div>
    </div>
  );
};

export default CircleButton;
