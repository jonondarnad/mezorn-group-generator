import React, { useEffect, useState } from "react";
import { isNumberInput } from "../utils";
import { Avatar } from "./Avatar";
import { Button } from "./Button";
import { List } from "./List";
import { ListItem } from "./ListItem";
import CircleButton from "../Components/CircleButton";
import { generateGroups } from "../utils/generator";
// const teamObject = {
//     teamName: string,
//     teamId: Number,
//     members: string[], user ids array,
//     score: Number,
// }

const GroupGenerator = ({ users = [] }) => {
    const [teamCount, setTeamCount] = useState("5");
    const [teams, setTeams] = useState([]);
    const [excluded, setExcluded] = useState([]);
    const [time, setTime] = useState(false);
    const [ balanceGender, setBalanceGender ] = useState(false);
    
    const handleTeamCountChange = (e) => {
        const { value } = e.target;
        if (isNumberInput(value)) {
            setTeamCount(value);
        }
    };

    useEffect(() => {
        if (time === true) {
            setTimeout(() => {
                setTime(false);
            }, 1000);
        }
    }, [time]);

    const generate = () => {
        setTime(true);
        const usersToParticipate = users.filter(
            (user) => !excluded.includes(user.userId)
        );
        if (teamCount && Number(teamCount) <= usersToParticipate.length) {
            const generatedTeams = generateGroups(usersToParticipate, Number(teamCount), balanceGender);
            
            setTeams(generatedTeams);
        }
    };

    const getUsersWithoutTeam = () => {
        const teamlessUsers = [];
        for (const user of users) {
            let exist = false;
            const { userId } = user;
            if (userId) {
                // find out if user is in group.
                for (const team of teams) {
                    for (const member of team?.members ?? []) {
                        if (member.userId === userId) {
                            exist = true;
                        }
                    }
                }
            }
            if (!exist) {
                teamlessUsers.push(user);
            }
        }
        return teamlessUsers;
    };

    const removeMember = (teamId, userId) => {
        const clone = [...teams];
        const teamIndex = clone.findIndex((team) => team.teamId === teamId);
        if (teamIndex > -1) {
            const team = { ...clone[teamIndex] };
            const members = [...team.members];
            const memberIndex = members.findIndex(
                (member) => member.userId === userId
            );
            if (memberIndex > -1) {
                members.splice(memberIndex, 1);
                team.members = members;
                clone[teamIndex] = team;
                setTeams(clone);
            }
        }
    };

    const handleReset = () => {
        setTeams([]);
    };

    const excludeUser = (userId) => {
        setExcluded([...excluded, userId]);
    };

    const includeUser = (userId) => {
        const clone = [...excluded];
        const index = clone.findIndex((id) => id === userId);

        if (index >= 0) {
            clone.splice(index, 1);
            setExcluded(clone);
        }
    };

    const handleGenderBalanceChange = () => {
        setBalanceGender(!balanceGender);
    }

    return (
        <div className={`bg-slate-900 h-full min-h-screen`}>
            <div className="flex flex-col md:flex-row justify-around">
                {/* Goliin heseg */}
                <div className="mx-1 my-1">
                    <div>
                        <div className="text-white">Багийн тоо</div>
                        <input
                            value={teamCount}
                            onChange={handleTeamCountChange}
                            type="number"
                            className="bg-slate-900 rounded-sm p-2 focus:border-none text-white outline-none border-b focus:border-b max-w-xs"
                        />
                    </div>
                    <div style={{ color: '#fff' }}>
                        Huiseer tentsuu huvaah
                        <input
                            type='checkbox'
                            checked={balanceGender}
                            onChange={handleGenderBalanceChange}
                        />
                    </div>
                    <div className="my-1"></div>
                    <Button onClick={generate}>generate</Button>
                    <Button onClick={handleReset}>reset</Button>
                </div>
                <List>
                    {getUsersWithoutTeam().map((user) => {
                        return (
                            <div
                                key={user.userId}
                                className={`flex justify-center items-center h-full`}
                            >
                                <ListItem url={user.photoUrl} gender={user.gender}>
                                    {user.firstName}
                                </ListItem>
                                {excluded.includes(user.userId) ? (
                                    <CircleButton onClick={() => includeUser(user.userId)}>
                                        Нэмэх
                                    </CircleButton>
                                ) : (
                                    <CircleButton minus onClick={() => excludeUser(user.userId)}>
                                        Хасах
                                    </CircleButton>
                                )}
                            </div>
                        );
                    })}
                </List>

                {/* Bagiin huvaarilalt */}
                <div className={`flex flex-wrap justify-center`}>
                    {time === false ? (
                        teams.map((team) => {
                            return (
                                <div
                                    key={team.teamId}
                                    className={`bg-blue-700 m-2 p-2 w-full max-w-xs`}
                                >
                                    <div style={{ textAlign: "center" }}>{team.teamName}</div>
                                    {team.members.map((member) => {
                                        return (
                                            <div key={member.userId} style={{ display: "flex" }}>
                                                <div style={{ flex: 1 }}>
                                                    {member.firstName} {member.lastName}
                                                </div>
                                                <button
                                                    onClick={() =>
                                                        removeMember(team.teamId, member.userId)
                                                    }
                                                >
                                                    del
                                                </button>
                                            </div>
                                        );
                                    })}
                                </div>
                            );
                        })
                    ) : (
                        <div>Unshaaal baina....</div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default GroupGenerator;
