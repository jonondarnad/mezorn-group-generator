import React from "react";

const List = ({ children }) => {
  return <div className={`w-full h-full  bg-slate-900 p-2`}>{children}</div>;
};

export { List };
