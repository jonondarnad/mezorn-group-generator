import React from "react";
import { Avatar } from "./Avatar";
const ListItem = ({ children, url, gender }) => {
  const female = `bg-gradient-to-r from-pink-400 to-red-500`;
  const male = `bg-gradient-to-r from-green-400 to-blue-500`;
  return (
    <div
      className={`text-sm text-white ${
        gender === "M" ? male : female
      } rounded-sm p-2 my-2 max-w-xs w-full mx-1 flex items-center h-full`}
    >
      <Avatar url={url} />
      <div className="w-1"></div>
      {children}
    </div>
  );
};

export { ListItem };
