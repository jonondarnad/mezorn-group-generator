import {
    collection,
    getFirestore,
    query,
    getDocs,
} from "firebase/firestore";
import { initializeApp } from "firebase/app";
const firebaseOkrConfig = {
    apiKey: "AIzaSyBhlllHMqam0FR6sx8oQBnY_DRJc5gyDew",
    authDomain: "okr-tracking-a4d11.firebaseapp.com",
    databaseURL: "https://okr-tracking-a4d11-default-rtdb.firebaseio.com",
    projectId: "okr-tracking-a4d11",
    storageBucket: "okr-tracking-a4d11.appspot.com",
    messagingSenderId: "557857784587",
    appId: "1:557857784587:web:7ed89b5282bfc08a88980e",
};
const app = initializeApp(firebaseOkrConfig);
const db = getFirestore(app);

export const selectAll = async (table) => {
    const q = query(collection(db, table));
    const querySnapShot = await getDocs(q);
    const array = [];
    querySnapShot.forEach((doc) => {
        array.push({ ...doc.data(), id: doc.id });
    });
    return array;
};
// export const selectBy = async (table, field, condition) => {
//     const array = [];
//     const q = query(
//         collection(db, table),
//         where(field, "==", condition),
//         where("isArchived", "==", false)
//     );
//     const docs = await getDocs(q);
//     docs.forEach((doc) => {
//         array.push({ ...doc.data(), id: doc.id });
//     });
//     return array;
// };
// export const selectById = async (table, id) => {
//     const snap = await getDoc(doc(db, table, id));
//     if (snap.exists()) {
//         return snap.data();
//     } else {
//         return "Not found";
//     }
// };
// export const selectDepartmentOkr = async (
//     table,
//     field,
//     condition,
//     company = null
// ) => {
//     const array = [];
//     const q = query(
//         collection(db, table),
//         where(field, "==", condition),
//         where("isArchived", "==", false),
//         where(
//             "level",
//             "in",
//             condition === "ceo" && company !== null
//                 ? ["company"]
//                 : condition === "ceo"
//                     ? ["person"]
//                     : ["company", "department"]
//         )
//     );
//     const docs = await getDocs(q);
//     docs.forEach((doc) => {
//         array.push({ ...doc.data(), id: doc.id });
//     });
//     return array;
// };