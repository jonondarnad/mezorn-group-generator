export const generateGroups = (usersToParticipate, teamCount = 1, balanceGender = false) => {
    if (balanceGender) {
        const teams = [];

        const male = [];
        const female = [];
        const unknown = [];

        for (let i = 0; i < teamCount; i++) {
            const teamId = i + 1;

            teams.push({
                teamId,
                teamName: `Team ${teamId}`,
                members: [],
                score: 0,
            });
        }

        for (const user of usersToParticipate) {
            if (user.gender === 'M') {
                male.push(user);
            } else if (user.gender === 'F') {
                female.push(user);
            } else {
                unknown.push(user);
            };
        };

        const genders = [
            male,
            female,
            unknown
        ];

        let index = 0;
        for (let i = 0; i < genders.length; i++) {
            const userList = genders[i];

            const minMembers = Math.floor(userList.length / teamCount);


            for (let j = 0; j < minMembers * teamCount; j++) {
                const teamIndex = index % 5;
                const randomIndex = Math.floor(Math.random() * userList.length);
                const user = userList[randomIndex];
                const team = { ...teams[teamIndex] };
                team.members = [ ...team.members, user ];
                teams[teamIndex] = team;
                userList.splice(randomIndex, 1);
                index++;
            }
        };

        // 
        for (const gender of genders) {
            for (const user of gender) {
                const availableTeamIndexes = getAvailableTeamIndexes(teams);
                const randomIndex = Math.floor(Math.random() * availableTeamIndexes.length);
                const randomTeamIndex = availableTeamIndexes[randomIndex];
                const team = { ...teams[randomTeamIndex] };
                team.members = [ ...team.members, user];
                teams[randomTeamIndex] = team;
            } 
        }

        return teams;
    }

    const count = Number(teamCount);
    const generatedTeams = [];
    const minMembers = Math.floor(usersToParticipate.length / count);
    const leftOvers = usersToParticipate.length - minMembers * count;

    const teamIndexes = Array.from(Array(count).keys());
    const leftOverIndexes = [];
    if (leftOvers > 0) {
        for (let i = 0; i < leftOvers; i++) {
            const randomIndex = Math.floor(Math.random() * teamIndexes.length); // get random index from teamIndexes (ali bagt iluu gishuudiig nemeh be gdg indexuud);
            leftOverIndexes.push(teamIndexes[randomIndex]);
            teamIndexes.splice(randomIndex, 1);
        }
    }

    const userIndexes = Array.from(Array(usersToParticipate.length).keys());
    // shuffling begins here...
    for (let i = 0; i < count; i++) {
        const teamId = i + 1;
        const members = [];

        for (let j = 0; j < minMembers; j++) {
            const randomIndex = Math.floor(Math.random() * userIndexes.length);
            members.push(usersToParticipate[userIndexes[randomIndex]]);
            userIndexes.splice(randomIndex, 1);
        }

        if (leftOverIndexes.includes(i)) {
            const randomIndex = Math.floor(Math.random() * userIndexes.length);
            members.push(usersToParticipate[userIndexes[randomIndex]]);
            userIndexes.splice(randomIndex, 1);
        }

        generatedTeams.push({
            teamId,
            teamName: `Team ${teamId}`,
            members,
            score: 0,
        });
    };
    return generatedTeams;
};

const getAvailableTeamIndexes = (teams) => {
    // returns available team indexes that can receive additional user.
    let minMemberCount = teams[0].members.length;
    for (const team of teams) {
        if (team.members.length < minMemberCount) {
            minMemberCount = team.members.length;
        }
    };

    const indexes = [];

    for (let i = 0; i < teams.length; i++) {
        const team = teams[i];
        if (team.members.length === minMemberCount) {
            indexes.push(i);
        }
    };
    return indexes;
}